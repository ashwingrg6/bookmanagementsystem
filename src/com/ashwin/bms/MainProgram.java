/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashwin.bms;

import com.ashwin.bms.controller.BookController;
import com.ashwin.bms.dao.BookDAO;
import com.ashwin.bms.dao.impl.BookDAOImpl;
import com.ashwin.bms.entity.Book;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author ashwin
 */
public class MainProgram {

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    BookDAO bookDAO = new BookDAOImpl();
    Scanner input = new Scanner(System.in);
    BookController bookController = new BookController(input);
    while(true){
      bookController.process();
    }
  }
  
}//End Class, MainProgram
