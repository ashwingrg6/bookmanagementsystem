/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashwin.bms.entity;

/**
 *
 * @author ashwin
 */
public class Book {
  
  private int id, price, noOfPages;
  private String title, description, issueDate, author;
  
  /**
   * Constructor
   */
  public Book(){
  }
  
  /**
   * Overloaded Constructor
   * @param id
   * @param price
   * @param noOfPages
   * @param title
   * @param description
   * @param issueDate
   * @param author 
   */
  public Book(int id, int price, int noOfPages, String title, String description, String issueDate, String author) {
    this.id = id;
    this.price = price;
    this.noOfPages = noOfPages;
    this.title = title;
    this.description = description;
    this.issueDate = issueDate;
    this.author = author;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public int getNoOfPages() {
    return noOfPages;
  }

  public void setNoOfPages(int noOfPages) {
    this.noOfPages = noOfPages;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getIssueDate() {
    return issueDate;
  }

  public void setIssueDate(String issueDate) {
    this.issueDate = issueDate;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  @Override
  public String toString() {
    return "Book{" + "id=" + id + ", price=" + price + ", noOfPages=" + noOfPages + ", title=" + title + ", description=" + description + ", issueDate=" + issueDate + ", author=" + author + '}';
  }
  
}
