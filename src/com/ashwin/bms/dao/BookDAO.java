/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashwin.bms.dao;

import com.ashwin.bms.entity.Book;
import java.util.List;

/**
 * BookDAO.java
 * Data Access Object class for Book
 * @author ashwin
 */
public interface BookDAO {
  
  public boolean insert(Book b);
  public boolean update(Book b);
  public boolean delete(int id);
  public Book getById(int id);
  public Book searchByTitle(String title);
  public List<Book> getAll();
  public void deleteAll();
  
}
