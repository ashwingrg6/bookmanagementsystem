/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashwin.bms.dao.impl;

import com.ashwin.bms.dao.BookDAO;
import com.ashwin.bms.entity.Book;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ashwin
 */
public class BookDAOImpl implements BookDAO{
  
  private List<Book> bookList;
  
  public BookDAOImpl(){
    bookList = new ArrayList<>();
  }
  
  /**
   * Get book details and add to book's arraylist.
   * @param book
   * @return boolean
   */
  @Override
  public boolean insert(Book book) {
    try {
      bookList.add(book);
      return true;
    } catch (Exception e){
      //e->getMessage()
      return false;
    }
  }

  /**
   * Get book details and update in book's arraylist accordingly.
   * @param book
   * @return boolean
   */
  @Override
  public boolean update(Book book) {
    int for_flag = 0;
    for(Book b: bookList){
      if(b.getId() == book.getId()){
//        bookList.get(for_flag).setTitle(book.getTitle());
//        bookList.get(for_flag).setAuthor(book.getAuthor());
//        bookList.get(for_flag).setDescription(book.getDescription());
//        bookList.get(for_flag).setIssueDate(book.getIssueDate());
//        bookList.get(for_flag).setNoOfPages(book.getNoOfPages());
//        bookList.get(for_flag).setPrice(book.getPrice());
        return true;
      }
    }
    return false;
  }//End function, update

  /**
   * Get book details as per requested book id and delete from book's array list if exists.
   * @param id
   * @return boolean
   */
  @Override
  public boolean delete(int id) {
    Book book = getById(id);
    if(book != null){
      bookList.remove(book);
      return true;
    }
    return false;
  }

  /**
   * Return book details as per requested book id.
   * @param id
   * @return boolean
   */
  @Override
  public Book getById(int id) {
    for(Book book: bookList){
      if(book.getId() == id){
        return book;
      }
    }
    return null;
  }

  /**
   * Return all books that are currently available.
   * @return List<Book>
   */
  @Override
  public List<Book> getAll() {
    return bookList;
  }
  
  /**
   * Return book as per requested book title.
   * @param title
   * @return mixed
   */
  @Override
  public Book searchByTitle(String title){
    for(Book b: bookList){
      if(b.getTitle().contains(title)){
        return b;
      }
    }
    return null;
  }
  
  /**
   * Delete all objects from collection.
   * @return void
   */
  @Override
  public void deleteAll(){
    bookList.clear();
//    bookList.removeAll(bookList); 
  }
  
}
