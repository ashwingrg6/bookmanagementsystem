/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashwin.bms.controller;

import com.ashwin.bms.dao.BookDAO;
import com.ashwin.bms.dao.impl.BookDAOImpl;
import com.ashwin.bms.entity.Book;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author ashwin
 */
public class BookController {
  
  private BookDAO bookDAO = new BookDAOImpl();
  private Scanner input;
  
  /**
   * Overloaded Constructor
   * @param input 
   */
  public BookController(Scanner input){
    this.input = input;
  }
  
  /**
   * Dispaly all the available operations along with its code.
   * @return void
   */
  public void displayMenu(){
    System.out.println("1. Register New Book");
    System.out.println("2. Delete Book");
    System.out.println("3. Show All Books");
    System.out.println("4. Search Book By ID");
    System.out.println("5. Search Book By Title");
    System.out.println("6. Update Book Info By Id.");
    System.out.println("7. Delete All Books.");
    System.out.println("8. Exit");
    System.out.println("Please Enter Your Choice [1-7]");
  }//End function, displayMenu
  
  /**
   * Get new book details from user and save the same details in store.
   * @return void
   */
  public void register(){
    Book book = new Book();
    System.out.println("Enter Book ID:");
    book.setId(input.nextInt());
    System.out.println("Enter Book Title:");
    book.setTitle(input.next());
    System.out.println("Enter Book's Description:");
    book.setDescription(input.next());
    System.out.println("Enter Book's Price");
    book.setPrice(input.nextInt());
    System.out.println("Enter Author Name:");
    book.setAuthor(input.next());
    System.out.println("Enter Book's Issue Date:");
    book.setIssueDate(input.next());
    System.out.println("Enter Book's Number of Pages:");
    book.setNoOfPages(input.nextInt());
    if(bookDAO.insert(book)){
      System.out.println("New Book has been registered successfully.");
    }
    else{
      System.out.println("OOPS! Something went wrong. Please try again later.");
    }
  }//End funciton, register
  
  /**
   * Display all the available books.
   * @return void
   */
  public void showAll(){
    ArrayList<Book> books = (ArrayList<Book>) bookDAO.getAll();
    if(books.isEmpty()){
      System.out.println("There are no books available right now.");
    }
    else {
      //looping through the all books
      bookDAO.getAll().forEach(b->{
        System.out.println(b);
      });
    }
  }//End function, showAll
  
  /**
   * Take the book id from user and delete accordingly.
   */
  public void delete(){
    System.out.println("Enter Book ID:");
    if(bookDAO.delete(input.nextInt())){
      System.out.println("Selected Book's details has been deleted successfully.");
    }
    else {
      System.out.println("Oops! Something went wrong. Please enter valid book id.");
    }
  }
  
  /**
   * Take book id from user and display book details if valid.
   */
  public void searchById(){
    System.out.println("Enter Book ID");
    Book book_search = bookDAO.getById(input.nextInt());
    if(book_search != null){
      System.out.println(book_search);
    }
    else {
      System.out.println("Oops! something went wrong. Please enter valid book id");
    }
  }
  
  /**
   * Search books in store as per the user input-book title.
   * @return void
   */
  public void searchByTitle(){
    System.out.println("Enter Book Title");
    Book searchBook = bookDAO.searchByTitle(input.next());
    if(searchBook != null){
      System.out.println(searchBook);
    }
    else {
      System.out.println("No matching books found."+"\n"+"Please try again with another title.");
    }
  }
  
  /**
   * Take book details and update the book info accordingly.
   * @return void
   */
  public void updateBook(){
    System.out.println("Enter Book ID");
    int updateId = input.nextInt();
    Book book_update = bookDAO.getById(updateId);
    if(book_update != null){ //if book is available as per user input-book id
      System.out.println("Book Details: "+book_update);
      Book bookUpdateInfo = new Book();
      bookUpdateInfo.setId(updateId);
      System.out.println("Enter Book Title:");
      bookUpdateInfo.setTitle(input.next());
      System.out.println("Enter Book's Description:");
      bookUpdateInfo.setDescription(input.next());
      System.out.println("Enter Book's Price");
      bookUpdateInfo.setPrice(input.nextInt());
      System.out.println("Enter Author Name:");
      bookUpdateInfo.setAuthor(input.next());
      System.out.println("Enter Book's Issue Date:");
      bookUpdateInfo.setIssueDate(input.next());
      System.out.println("Enter Book's Number of Pages:");
      bookUpdateInfo.setNoOfPages(input.nextInt());
      if(bookDAO.update(bookUpdateInfo)){//update book details
        System.out.println("Selected Book's details has been updated successfully.");
      }
      else{
        System.out.println("Oops! something went wrong. Please enter valid book id");
      }
    }
    else {
      System.out.println("Oops! something went wrong. Please enter valid book id");
    }
  }//End funciton, updateBook
  
  /**
   * Delete all available books from store.
   * @return void
   */
  public void deleteAllBooks(){
    System.out.println("Are you sure you want to delete all books? y/n");
    //prompting user to confirm to delete all books
    if(input.next().equalsIgnoreCase("y")){
      bookDAO.deleteAll();
      System.out.println("All books are deleted successfully.");
    }
    else {
      System.out.println("You have cancelled to delete all books.");
    }
  }//End funciton, deleteAllBooks
  
  /**
   * Take user input- operation-code and process accordingly.
   * @return void
   */
  public void process(){
    displayMenu();
    int choice = input.nextInt();
    //user's choice
    switch(choice){
      case 1:
        register();
        break;
      case 2:
        delete();
        break;
      case 3:
        showAll();
        break;
      case 4:
        searchById();
        break;
      case 5:
        searchByTitle();
        break;
      case 6:
        updateBook();
        break;
      case 7:
        deleteAllBooks();
        break;
      case 8:
        System.exit(0);
        break;
      default:
        System.out.println("Please enter valid code/in number.");
        break;
    }//End switch, choice
  }//End function, process
  
}//End Class, BookController
